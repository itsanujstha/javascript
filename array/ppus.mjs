//push, pop, unShift, shift

let ar = [9,10,11]

// ar.push(9)
// console.log(ar)    //[9, 10, 11, 9]

// ar.pop()
// console.log(ar)    //[9, 10]

// ar.unshift(100)
// console.log(ar) //[100, 9, 10, 11]

ar.shift()
console.log(ar) //[10, 11]