let currentDate = new Date()
console.log(currentDate)
// is yyyy-mm-ddThh:mm:ss
//iso format 2024-01-05T10:40:25.075Z
//must have 4 digit in year 2 digit in month and so on


//date and time in a readable format

let dateTime = new Date().toLocaleString();
console.log(dateTime) //return in local time

let date = new Date().toLocaleDateString()
console.log(date)   

let time = new Date().toLocaleTimeString()
console.log(time)