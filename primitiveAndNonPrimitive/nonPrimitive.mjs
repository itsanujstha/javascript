//array, object ( are non primitive)
let a = [1,2]
let b = [1,2]
let c = a

//In case of nonPrimitive "===="" checks whether the address(memory location) are same, and if the address is same returns True
console.log(a===b) // F
console.log(a===c) // T

let ar1 = {name: "nitan"}

console.log(typeof ar1) // object
//the type of non primitive is object
