{
    let a = 1
    {
        let a = 5
    }
}

// we can not define same variable(name) in same block
// but we can define same variable(name) in different block