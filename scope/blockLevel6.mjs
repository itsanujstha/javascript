let a  = 3

{
    //A

    console.log(a)
    let a = 8
}

//console.log(a) will result error because the variable will be searched in A block (but the scope of 
//block a is from line number 7)