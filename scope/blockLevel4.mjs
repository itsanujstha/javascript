


{
    //A
let a = 3
{//B
    let b = 7

    {
        //c
        a = 10//10
        console.log(a)//10
    }

    console.log(a) //10
}
console.log(a) //10
}

//While changing variable
//first it searches variable in its own block
//if it is unable to find it in its own block then it proceeds to find the variable in its parent's block and so on.
//and this behavior is called scope chaining