let a = 3
let c = 10
{
    //A
    let a = 4
    let b = 10
    console.log(a)
}
console.log(a)

//when you execute a program
//it has two phases:
//1) memory allocation (it is done first)
//2) code execution (it is done after memory allocation)

//Global
//a = 3
//c == 10

//A
//a=4
//b=10