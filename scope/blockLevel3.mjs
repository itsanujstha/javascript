


{
        //A
    let a = 1
    {//B
        let a = 5
        console.log(a)
    }
    console.log(a)
}

//While calling variable
//first it searches variable in its own block
//if it is unable to find it in its own block then it proceeds to find the variable in its parent's block and so on.
//and this behavior is called scope chaining