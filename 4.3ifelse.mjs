if (true)
{
    console.log("I am if block")
}
else 
{
    console.log("I am else block")
}

//else block will execute if none of the block above it gets activated