
//Crate array out of object's keys or values
let obj1 = {
    name: "nitan",
    age: 29
}

let keysArr = Object.keys(obj1)
console.log(keysArr)    //[ 'name', 'age' ]

let valuesArr = Object.values(obj1)
console.log(valuesArr)  //[ 'nitan', 29 ]

let prValue = Object.entries(obj1)
console.log(prValue)