let name = " my name is anUj"

console.log(name.length)
console.log(name.toUpperCase())
console.log(name.toLowerCase())

//trim
console.log(name.trimStart())
console.log(name.trimEnd())
console.log(name.trim())

//includes
console.log(name.includes("anf"))
console.log(name.startsWith(" an"))
console.log(name.endsWith("j"))

//replace
console.log(name.replaceAll(" ", ","))

