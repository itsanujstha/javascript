// spread operator => wrapper opener (it is used to make new variable(data)) ( it is used in giving section)
// rest operator => takes rest of value(it is used in receiving section)

let a1 = [1,2,3]
let a2 = [5,10,...a1,11]
console.log(a2)//[5,10,1,2,3,11]


let [a, ...b] = [1,2,3] // example of rest operator