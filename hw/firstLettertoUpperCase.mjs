//capitalize the first letter of a word using map method

let firstLetterCapital = (input) => {
    //input =my => ["m","y"] =>["M","y"] ="My"
    let inputAr = input.split(""); //["m","y"]
    let outputAr = inputAr.map((value, i) => {
      if (i === 0) {
        return value.toUpperCase();
      } else {
        return value.toLowerCase();
      }
    }); //["M","y"]
    let output = outputAr.join("");
    return output;
  };
  //console.log(firstLetterCapital("my"));

  let inputWords = (input) => {
  
    let splittedInputWords = input.split(" ") //["this", "is", "anuj", "shrestha"]

  let processedArr = splittedInputWords.map((value,i)=>{
    return firstLetterCapital(value)
  })

  let joinedProcessedArr = processedArr.join(" ") //This Is Anuj Shrestha 
  return joinedProcessedArr
 }
  console.log(inputWords("this is anuj shrestha"))


