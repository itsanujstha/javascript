let firstLetterCapital = (input) => {
    //input = "my"

    let inputAr = input.split("") //["m", "y"]
    inputAr[0] = inputAr[0].toUpperCase()
    //console.log(inputAr)//["M","y"]

    let output = inputAr.join("")//"My"
    return output
    
}

//console.log(firstLetterCapital("my"))


let IsWordCapital = (input) => {
    //input = "my name is nitan"

    let inputAr = input.split(" ") //["my", "name", "is", "nitan"]

    //outputAr = ["My", "Name", "Is", "Nitan"]
    let outputAr = inputAr.map((value,i)=>{
        return firstLetterCapital(value) //"Is"
    })
    let output = outputAr.join(" ")
    return output
}

console.log(IsWordCapital("my name"))
