// make a arrow function that take input as array and return , output in ascneding order
// let arr = [6, 10, 3, 1, 13]
// let arr2 = arr.sort()
// console.log(arr2)   // [1, 10, 13, 3, 6]

// 		make a arrow function that take input as array and return , output in desending order
// let arr = [6, 10, 3, 1, 13]
// let arr2 = arr.sort().reverse()
// console.log(arr2)    // [6, 3, 13, 10, 1]

// 		make a arrow function that take input as [1,2,3] and return [1,8,3] here ar[1] is changed from 2 to 8
// let arr = [1,2,3]
// let arr2 = arr.map((value,i) => {
//     if(value%2===0){
//         return value*4
//     }
//     else{
//         return value
//     }
// })
// console.log(arr2) //[1,8,3]

// 		make a arrow function which takes input as [1,2,3] and produce output as "1,2,3"
// let arr1 = [1,2,3]
// let arr2 = arr1.join(",")
// console.log(arr2) //1,2,3

//  make a arrow function that take input as [1,2,3] , and return [1,2,3,4] by using push methode\
// let arr1 = [1,2,3]
// arr1.push(4)
// console.log(arr1)

// 	make a arrow function that takes input as array of number and return the third largest number
let arr1 = [3,20,10,30]

arr1.sort((a,b)=>(a-b))

console.log(arr1)
