export let convertToUpper = (value) => {
    
    return value.toUpperCase()

}

export let convertToLower = (value) => {
    
    return value.toLowerCase()

}

export let trim = (value) => {
    
    return value.trim()

}

export let included = (value) => {
    
    return value.includes("Bearear")

}

export let stringReplace = (value) => {

    return value.replaceAll("Nitan", "Ram")
}

export let checkAdmin = (value) => {

    return value.includes("admin")
}

export let toLowerAndTrim = (value) => {
    

    return value.trim(value.toLowerCase())
    
}
