//string, number, boolean

//conversion from number to string.
// let age = 40
// let ageStr = String(age)    //"40"
// console.log(ageStr)

//conversion from string to number
// let age = "40"
// let ageNum = Number(age)    //40
// console.log(ageNum)

//Conversion from any to boolean.

console.log(Boolean(""))
console.log(Boolean("a"))
